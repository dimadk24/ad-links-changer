import os
import sys
from time import sleep
from typing import Dict, Union

import requests
from decouple import config

api_url = 'https://api.vk.com/method/'
api_version = "5.80"
access_token: str = config('access_token').strip()
account_id: int = config('account_id', cast=int)
client_id: str = config('client_id').strip()
rucaptcha_key: str = config('rucaptcha_key').strip()
short_links: bool = bool(config('short_links', default=1, cast=int))
proxy_url: str = config('proxy_url', default='').strip()
if proxy_url and (not (proxy_url.startswith('http://') or proxy_url.startswith('https://'))):
    proxy_url = 'http://' + proxy_url
del config

https = 'https://'

print('Поддержка:', 'vk.com/dimadk24', 'vk.com/id159204098', 't.me/dimadk24', sep='\n', end='\n\n')


def close(success: bool) -> None:
    if success:
        text = 'успешно завершен'
    else:
        text = 'завершен НЕ успешно'
    print('Скрипт {success}. Нажми Enter для выхода или закрой окно'.format(success=text))
    input()
    sys.exit()


if not access_token:
    print('Отсутствует access_token. Читай ReadMe, чтобы узнать как его установить')
    close(False)
if not account_id:
    print('Отсутствует account_id. Читай ReadMe, чтобы узнать как его установить')
    close(False)
if not rucaptcha_key:
    print('Отсутствует rucaptcha_key')
    close(False)


class BadImageSize(Exception):
    pass


class Post:
    def __init__(self, post_id: int, owner_id: int, text: str, attachments: Dict, geo: Dict) -> None:
        self.post_id = post_id
        self.owner_id = owner_id
        self.text = text
        self.attachments = attachments
        self.vk_attachments = ''
        self.link_button = ''
        self.link_title = ''
        self.link_image = ''
        self.geo = geo
        self.lat = None
        self.long = None
        self.place_id = None
        if geo:
            place = geo.get('place')
            if place:
                self.lat = place['latitude']
                self.long = place['longitude']
                self.place_id = place['id']

    def __str__(self) -> str:
        return f'vk.com/wall{self.owner_id}_{self.post_id}'

    def __repr__(self) -> str:
        return f'Пост: {self.post_id}\nТекст: {self.text}\nПриложения: {self.attachments}'

    @property
    def contains_link(self) -> bool:
        return 'http://' in self.text or 'https://' in self.text

    def change_text(self, url: str) -> None:
        if not (url.startswith('http://') or url.startswith('https://')):
            url = 'http://' + url
        part = self.text
        self.text = ''
        while True:
            link_start_pos = min([part.find('http://'), part.find('https://')])
            if link_start_pos == -1:
                link_start_pos = max([part.find('http://'), part.find('https://')])
                if link_start_pos == -1:
                    self.text += part
                    break
            i = link_start_pos
            while i < len(part) and (part[i].isalpha() or part[i].isdigit() or part[i] in './_-?#%&()+=*:'):
                i += 1
            link_stop_pos = i
            self.text += part[:link_start_pos] + url
            part = part[link_stop_pos:]

    def change_link(self, url: str) -> int:
        if not self.attachments:
            return 0
        link_changed = False
        button_changed = False
        for attach in self.attachments:
            if attach['type'] == 'link':
                link = attach['link']
                link['url'] = url
                link_changed = True
                if link.get('button'):
                    link['button']['action']['url'] = url
                    button_changed = True
        if link_changed and button_changed:
            return 3
        elif link_changed:
            return 2
        else:
            return 1

    def create_vk_attaches(self) -> None:
        vk_attaches = ''
        if self.attachments:
            for attach in self.attachments:
                attach_type = attach['type']
                inner_attach = attach[attach_type]
                if attach_type != 'link':
                    vk_attaches += attach_type + str(inner_attach['owner_id']) + '_' + str(inner_attach['id']) + ','
                else:
                    url: str = inner_attach['url']
                    if url.startswith('http://') or url.startswith('https://'):
                        added_protocol = ''
                    else:
                        added_protocol = 'http://'
                    vk_attaches += added_protocol + url + ','
                    self.link_title = inner_attach['title']
                    button = inner_attach.get('button')
                    if button:
                        self.link_button = button['action']['type']
                    photo = inner_attach.get('photo')
                    if photo:
                        try:
                            self.link_image = get_max_photo_url(photo['sizes'])
                        except KeyError:
                            raise BadImageSize
        self.vk_attachments = vk_attaches[:-1]

    def save(self) -> int:
        data = {'owner_id': self.owner_id, 'post_id': self.post_id, 'message': self.text,
                'attachments': self.vk_attachments, 'lat': self.lat, 'long': self.long, 'place_id': self.place_id,
                'link_button': self.link_button, 'link_title': self.link_title, 'link_image': self.link_image}
        new_data = {}
        for key in data:
            if data[key]:
                new_data.update({key: data[key]})
        return vk_request('wall.editAdsStealth', **new_data)


def get_max_photo_url(sizes: Dict) -> str:
    for photo_size in sizes:
        if photo_size.get('width') == 537 and photo_size.get('height') == 240:
            return photo_size['url']
    raise KeyError


def handle_captcha_service_error(error: str) -> bool:
    stop_errors = {
        'ERROR_WRONG_USER_KEY': 'Неверный ключ доступа к сервису решения капч. Возьми его с '
                                'https://rucaptcha.com/setting',
        'ERROR_KEY_DOES_NOT_EXIST': 'Неверный ключ доступа к сервису решения капч. Возьми его с '
                                    'https://rucaptcha.com/setting',
        'ERROR_ZERO_BALANCE': 'Недостаточно денег на счете сервиса решения капч',
        'ERROR_IP_NOT_ALLOWED': 'Проверь список адресов: https://rucaptcha.com/iplist',
        'IP_BANNED': 'IP-адрес забанен, обратись в поддержку сервиса или ко мне',
        'MAX_USER_TURN': 'Слишком частые обращения к сервису решения капч',
        'ERROR_WRONG_ID_FORMAT': 'Неверный формат id капчи',
        'ERROR_WRONG_CAPTCHA_ID': 'Неверный id капчи',
        'ERROR: 1001': 'Сервис по решению капчи заблокировал нас на 10 мин, подожди их',
        'ERROR: 1002': 'Сервис по решению капчи заблокировал нас на 5 мин, подожди их',
        'ERROR: 1003': 'Сервис по решению капчи заблокировал нас на 30 сек, подожди их',
        'ERROR: 1004': 'Сервис по решению капчи заблокировал нас на 10 мин, подожди их',
        'ERROR: 1005': 'Сервис по решению капчи заблокировал нас на 5 мин, подожди их'
    }
    bad_captcha_errors = {
        'ERROR_NO_SLOT_AVAILABLE': 'Повысь максимальную ставку за решение капчи ( https://rucaptcha.com/setting ), '
                                   'или подожди',
        'ERROR_ZERO_CAPTCHA_FILESIZE': 'Ошибка с файлом капчи',
        'ERROR_TOO_BIG_CAPTCHA_FILESIZE': 'Ошибка с файлом капчи',
        'ERROR_WRONG_FILE_EXTENSION': 'Ошибка с файлом капчи',
        'ERROR_IMAGE_TYPE_NOT_SUPPORTED': 'Ошибка с файлом капчи',
        'ERROR_CAPTCHA_UNSOLVABLE': 'Капча нерешаема, повторяю запрос',
        'ERROR_BAD_DUPLICATES': 'Не было достигнуто требуемого количества совпадений капчи от работников, повторяю '
                                'запрос'
    }
    if error in stop_errors:
        print(stop_errors[error])
        close(False)
    elif error in bad_captcha_errors:
        print(bad_captcha_errors[error])
        return False
    elif error == 'CAPCHA_NOT_READY':
        print('Капча еще не решена. Жду 5 секунд')
        sleep(5)
        return True
    else:
        print('Произошла неизвестная ошибка сервиса решения капч, повторяю запрос', error, sep='\n')
        return False


def solve_captcha(img_url: str) -> Union[bool, str]:
    r = requests.get(img_url)
    with open('temp_captcha.jpg', 'wb') as f:
        f.write(r.content)
    while True:
        r = requests.post('http://rucaptcha.com/in.php',
                          data={'key': rucaptcha_key, 'json': 1,
                                'method': 'post', 'regsense': 1, 'numeric': 4},
                          files={'file': open('temp_captcha.jpg', 'rb')})
        if r.status_code == requests.codes.ok:
            break
        else:
            print('Упал сервис по решению капчи, жду 20 секунд и повторяю запрос')
            sleep(20)
    os.remove('temp_captcha.jpg')
    json_response = r.json()
    if not json_response['status']:
        error_response = handle_captcha_service_error(json_response['request'])
        if not error_response:
            return False
    else:
        print('Капча успешно загружена на rucaptcha. Жду 5 секунд и пробую получить ее решение')
        captcha_id = json_response['request']
        sleep(5)
        while True:
            r = requests.get('http://rucaptcha.com/res.php', params={'key': rucaptcha_key, 'action': 'get',
                                                                     'id': captcha_id, 'json': 1})
            if r.status_code != requests.codes.ok:
                print('Упал сервис по решению капчи, жду 20 секунд и повторяю запрос')
                sleep(20)
                continue
            json_response = r.json()
            if not json_response['status']:
                error_response = handle_captcha_service_error(json_response['request'])
                if not error_response:
                    return False
            else:
                break
        print('Капча решена, повторяю завпрос')
        return json_response['request']


def vk_request(relative_url: str, **kwargs):
    retry_errors = {
        6: 'Слишком много запросов в секунду',
        9: 'Слишком много однотипных действий'
    }
    stop_errors = {
        1: 'Произошла неизвестная для ВК ошибка. Попробуй запустить скрипт позже',
        5: 'Авторизация не удалась. Нужно получить новый access_token',
        10: 'Произошла внутренняя ошибка сервера ВК. Попробуй запустить скрипт позже',
        203: 'Доступ к группе запрещён',
        224: 'Слишком много рекламных постов. Странная ошибка вк. Попробуй запустить скрипт заново',
        600: 'Нет прав на выполнение данных операций с рекламным кабинетом',
        601: 'Отказано в доступе. Вы превысили ограничение на количество запросов за день. Попробуйте позже.',
        603: 'Произошла ошибка при работе с рекламным кабинетом'
    }
    requests_params = {
        'data': {
            **kwargs,
            'access_token': access_token,
            'v': api_version
        }
    }
    if proxy_url:
        requests_params.update({'proxies': {
            'http': proxy_url,
            'https': proxy_url
        }})
    response = requests.post(api_url + relative_url, **requests_params)
    if not response.status_code == requests.codes.ok:
        print('Ошибка ответа вк. Подожди немного')
    else:
        json_response = response.json()
        if 'error' in json_response:
            error_code = json_response['error']['error_code']
            if error_code in retry_errors:
                return vk_request(relative_url, **kwargs)
            elif error_code in stop_errors:
                print(stop_errors[error_code])
                close(False)
            elif error_code == 14:  # captcha_error
                print('Капча. Решаю ее')
                captcha_key = solve_captcha(json_response['error']['captcha_img'])
                if not captcha_key:
                    return vk_request(relative_url, **kwargs)
                else:
                    kwargs.update({
                        'captcha_sid': json_response['error']['captcha_sid'],
                        'captcha_key': captcha_key
                    })
                    return vk_request(relative_url, **kwargs)
            else:
                print('Возникла неизвестная ошибка: ' + json_response['error']['error_msg'])
                close(False)
        else:
            return json_response['response']


def short_link(url: str) -> str:
    if url.startswith('https://vk.cc') or url.startswith('http://vk.cc') or url.startswith('vk.cc'):
        return url
    short_url = vk_request('utils.getShortLink', url=url, private=1)['short_url']
    return short_url

# Get ad_ids from file
ad_ids = []
search_text = 'union_id'
with open('ads.txt') as f:
    for line in f:
        link = line.strip()
        if link == '':
            continue
        if search_text not in link:
            print('Неверная ссылка "{link}". В ней нету параметра "union_id"'.format(link=link))
            close(False)
        ad_ids.append(link[link.rfind(search_text) + len(search_text) + 1:])
print('Всего объявлений для изменения:', len(ad_ids))

# Get links from file
links = []
with open('links.txt') as f:
    for line in f:
        link = line.strip()
        if link == '':
            continue
        links.append(link)
print('Всего новых ссылок:', len(links))
del f

# Check length of links and ad_ids
if not len(ad_ids) == len(links):
    print('Количество объявлений не равно количеству ссылок')
    close(False)

# Get ads layout
vk_ad_ids = '['
for ad_id in ad_ids:
    vk_ad_ids += ad_id + ','
vk_ad_ids = vk_ad_ids[:-1] + ']'
params = {'account_id': account_id,
          'ad_ids': vk_ad_ids}
if client_id:
    params.update({'client_id': client_id})
ads = vk_request('ads.getAdsLayout', **params)

# Sort ads and get posts out of them
new_ads = []
for ad_id in ad_ids:
    appended = False
    for ad in ads:
        if ad['id'] == ad_id:
            new_ads.append(ad)
            ads.remove(ad)
            appended = True
            break
    if appended:
        continue
ads = new_ads
del new_ads
posts = []
for ad in ads:
    if ad['ad_format'] == 9:
        posts.append(ad['link_url'][ad['link_url'].find('wall') + len('wall'):])

if not len(posts) == len(links):
    print('Количество постов, полученных из объявлений, не равно количеству ссылок.',
          'Вероятно где-то ссылка на рекламное объявление не формата запись')
    close(False)

# Convert posts into vk format and get posts data
vk_posts = ''
for i in range(0, len(posts), 100):
    for post in posts[i: i + 100]:
        vk_posts += post + ','
    vk_posts = vk_posts[:-1]
    r = vk_request('wall.getById', posts=vk_posts)
    posts = []
    for post in r:
        posts.append(Post(post['id'], post['owner_id'], post.get('text'), post.get('attachments'), post.get('geo')))
del vk_posts

# Change links
i = 0
for post in posts:  # type: Post
    link = links[i]
    if post.contains_link:
        if short_links:
            link = short_link(link)
        post.change_text(link)
        change_link_response = post.change_link(link)
        if change_link_response == 2:
            text = 'Изменил текст и сниппет'
        elif change_link_response == 3:
            text = 'Изменил текст и кнопку'
        else:
            text = 'Изменил текст'
    else:
        change_link_response = post.change_link(link)
        if change_link_response == 2:
            text = 'Изменил сниппет'
        elif change_link_response == 3:
            text = 'Изменил кнопку'
        else:
            text = 'Пропустил пост, НЕ перехожу к следующей ссылке'
            i -= 1
    try:
        post.create_vk_attaches()
    except BadImageSize:
        print(f'Возникла ошибка с получение картинки сниппета поста {post},\n'
              'отправь мне ссылку на него. А пока я его пропущу,\n'
              'не переходя к следующей ссылке')
        continue
    post.save()
    print(i + 1, text)
    i += 1
close(True)
